const express = require('express');
const apiRouter = express.Router();

  var munchery = require('../controllers/munchery');

// Munchery Routes
  apiRouter.route('/platerak')
    .get(munchery.findAllPlaterak)
    .post(munchery.addPlatera);

  
  apiRouter.route('/platera/:id')
    .get(munchery.findPlateraById)
    .put(munchery.updatePlatera)
    .delete(munchery.deletePlatera);



/* GET api listing. */
apiRouter.get('/', (req, res) => {
  res.send('api works');
});


module.exports = apiRouter;