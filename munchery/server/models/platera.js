//Require mongoose package
const mongoose = require('mongoose');

const platerakSchema = mongoose.Schema({    
	nombre: String,    	
	precio: Number,    	
	imagen: String,    	
	stock: Number,    	
	data: Date 
},
{ collection: 'platerak' });

const platerak = module.exports = mongoose.model('platerak', platerakSchema );
