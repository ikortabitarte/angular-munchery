/* Zein Schema erabiliko dugun, kolekzio batekin lotuta*/
const platerak = require('../models/platera');

//GET - Return all Platerak in the DB
exports.findAllPlaterak = function(req, res) {    
	platerak.find(function(err, platerak) {		
		if (err) return res.send(500, err.message);		
		console.log('GET /platerak');		
		res.status(200).jsonp(platerak);	
	});
};
//GET - Retun one platera in the DB
exports.findPlateraById = function(req, res) {
    platerak.findById(req.params.id, function(err, platera) {
    	if (err) return res.send(500, err.message);
		console.log('GET /platera/' + req.params.id);
		res.status(200).jsonp(platera);
    });
};
//POST - Add one platera to the DB
exports.addPlatera = function(req, res) {
	console.log('POST');	
	console.log(req.body);	
	var platera = new platerak({		
		nombre: req.body.nombre,		
		precio: req.body.precio,		
		imagen: req.body.imagen,		
		stock: req.body.stock,		
		data: req.body.data,	
	});	
	platera.save(function(err, platera) {		
		if (err) return res.status(500).send(err.message);		
		console.log("Plater berria gorde da");		
		res.status(200).jsonp(platera);	
	});
};
//PUT - Update one platera
exports.updatePlatera = function(req, res) {	
platerak.findById(req.params.id, function(err, platera) {		
	platera.nombre = req.body.nombre;		
	platera.precio = req.body.precio;		
	platera.imagen = req.body.imagen;		
	platera.stock = req.body.stock;		
	platera.data = req.body.data;		
	platera.save(function(err) {			
		if (err) return res.status(500).send(err.message);											
		res.status(200).jsonp(platera);		
		});	
	});
};
//DELETE - Delete one platera.
	exports.deletePlatera = function(req, res) {	
	platerak.findById(req.params.id, function(err, platera) {									
		platera.remove(function(err) {			
			if (err) return res.status(500).send(err.message);										
			res.status(200).send();		
		})	
	});
};




