import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';


import { AppComponent } from './app.component';
import { MenuaComponent } from './menua/menua.component';
import { AppRoutingModule } from './app-routing/app-routing.module';
import { HomeComponent } from './home/home.component';
import { PlateraService } from './platera.service';
import { CartService } from './cart.service';
import { CartListComponent } from './cart-list/cart-list.component';
import { PlaterakKudeatuComponent } from './platerak-kudeatu/platerak-kudeatu.component';
import { BerriaComponent } from './berria/berria.component';
import { PlateraDetaileaComponent } from './platera-detailea/platera-detailea.component';
import { AldatuComponent } from './aldatu/aldatu.component';


@NgModule({
  declarations: [
    AppComponent,
    MenuaComponent,
    HomeComponent,
    CartListComponent,
    PlaterakKudeatuComponent,
    BerriaComponent,
    PlateraDetaileaComponent,
    AldatuComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule,
    AppRoutingModule
  ],
  providers: [PlateraService, CartService],
  bootstrap: [AppComponent]
})
export class AppModule { }
