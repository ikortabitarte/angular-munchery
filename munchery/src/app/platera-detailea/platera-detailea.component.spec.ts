import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PlateraDetaileaComponent } from './platera-detailea.component';

describe('PlateraDetaileaComponent', () => {
  let component: PlateraDetaileaComponent;
  let fixture: ComponentFixture<PlateraDetaileaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PlateraDetaileaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PlateraDetaileaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
