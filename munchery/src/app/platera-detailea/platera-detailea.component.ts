import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { PlateraService } from './../platera.service';
import { Platera } from './../platera';


@Component({
  selector: 'app-platera-detailea',
  templateUrl: './platera-detailea.component.html',
  styleUrls: ['./platera-detailea.component.css']
})
export class PlateraDetaileaComponent implements OnInit {

  constructor(private route: ActivatedRoute, private plateraService: PlateraService) { }

  platera: Platera;  
  
  getPlatera(): void {     
	  var id = this.route.snapshot.paramMap.get('id');       	
	  this.plateraService.PlaterBat(id)          		
		  .subscribe(platera=> {this.platera = platera},
		            error => console.log("Error :: " + error));  	
	}  


  ngOnInit() {
    this.getPlatera();
  }

}
