import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { PlateraService } from './../platera.service';
import { Platera } from './../platera';


@Component({
  selector: 'app-aldatu',
  templateUrl: './aldatu.component.html',
  styleUrls: ['./aldatu.component.css']
})
export class AldatuComponent implements OnInit {

  constructor(private route: ActivatedRoute, private plateraService: PlateraService) { }

  platera: Platera;
  
  getPlatera(): void {     
  	var id = this.route.snapshot.paramMap.get('id');       	
  	this.plateraService.PlaterBat(id)          		
  		.subscribe(platera=> {this.platera = platera},          					  				  error => console.log("Error :: " + error));  	
  	}
  	
  PlateraAldatu(): void {    
  this.plateraService.EditPlatera(this.platera)    
  	.subscribe(res => {console.log(res);},        
  				err => {console.log("Errorea gertatu da");}    
  				);    
  	}

  ngOnInit() {
    this.getPlatera();
  }

}
