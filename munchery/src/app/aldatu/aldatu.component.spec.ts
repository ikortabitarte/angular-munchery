import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AldatuComponent } from './aldatu.component';

describe('AldatuComponent', () => {
  let component: AldatuComponent;
  let fixture: ComponentFixture<AldatuComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AldatuComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AldatuComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
