import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {Observable} from 'rxjs/Observable';
import { Subject } from 'rxjs/Subject';
import { Platera } from './platera';
import { CartState } from './cart-state';


@Injectable()
export class CartService {

  constructor(private http: HttpClient) { }

    private cartSubject = new Subject<CartState>();    
	Platerak : Platera[]= [];    
	CartState = this.cartSubject.asObservable();        

	PlateraGehitu(platera:any) {      
		console.log('in service');      
		this.Platerak.push(platera)      
		this.cartSubject.next(<CartState>{loaded: true, platerak:  this.Platerak});    


    }
}
