import { Component, OnInit, OnDestroy } from '@angular/core';
import { Platera } from './../platera';
import {CartService} from './../cart.service';
import {Subscription} from 'rxjs/Subscription';
import {CartState} from './../cart-state';


@Component({
  selector: 'app-cart-list',
  templateUrl: './cart-list.component.html',
  styleUrls: ['./cart-list.component.css']
})
export class CartListComponent implements OnInit {
  loaded : boolean = true    
	platerak : Platera[];    
	private subscription : Subscription;      


  constructor(private cartService : CartService) { }

  ngOnInit() {
    
    this.subscription = this.cartService.CartState.subscribe((state : CartState) => {                			this.platerak = state.platerak;                
			console.log(this.platerak);            
		}); 
  }
  ngOnDestroy() {      
		this.subscription.unsubscribe();  
	}

}
