import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import {Observable} from 'rxjs/Observable';
import { Platera } from './platera';


@Injectable()
export class PlateraService {

  constructor(private http: HttpClient) { }

    private Url = '/api';    
    ListPlaterak(): Observable<Platera[]> {        
	    let ListUrl = `${this.Url}/platerak/`;		
	    return this.http.get<Platera[]>(ListUrl)		    
	 }
	 
	 PlaterBat(id: any): Observable<Platera> {
	 let ListUrl = `${this.Url}/platera/`;        
	return this.http.get<Platera>(ListUrl+ id)    
    }
    
    AddPlatera(platera: Platera) {        
			let ListUrl = `${this.Url}/platerak/`;        
			const headers = new HttpHeaders();         
			const body = {nombre: platera.nombre, precio: platera.precio, imagen: platera.imagen, stock: platera.stock, data: platera.data};         
			console.log(body);        	
			headers.append('Content-Type', 'application/json');        
			return this.http.post(ListUrl, body ,{headers: headers})    
	}
	
	EditPlatera(platera: Platera) {        
		let ListUrl = `${this.Url}/platera/${platera._id}`;
		const headers = new HttpHeaders();         
		const body = {nombre: platera.nombre, precio: platera.precio, imagen: platera.imagen, stock: platera.stock, data: platera.data};         
		console.log(body);        	
		headers.append('Content-Type', 'application/json');        
		return this.http.put(ListUrl, body ,{headers: headers})    
		
	}
	DeletePlatera(id) {        
	let ListUrl = `${this.Url}/platera/${id}`;        
	return this.http.delete<Platera>(ListUrl);    
	}


}
