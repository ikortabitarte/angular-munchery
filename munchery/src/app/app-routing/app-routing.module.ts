import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';

import { MenuaComponent } from '../menua/menua.component';
import { HomeComponent } from '../home/home.component';
import { PlaterakKudeatuComponent } from '../platerak-kudeatu/platerak-kudeatu.component';
import { BerriaComponent } from '../berria/berria.component';
import { PlateraDetaileaComponent } from '../platera-detailea/platera-detailea.component';
import { AldatuComponent } from '../aldatu/aldatu.component';


const routes: Routes = [        
		{ path: 'menua', component: MenuaComponent},
		{ path: 'home', component: HomeComponent},
		{ path: 'platera/:id', component: PlateraDetaileaComponent},
		{ path: 'aldatu/:id', component: AldatuComponent},
		{ path: 'platerakKudeatu', component: PlaterakKudeatuComponent},
		{ path: 'berria', component: BerriaComponent},
		{ path: '', component: HomeComponent}
];    

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forRoot(routes)        
  ],
  exports: [            
		RouterModule        
	],        

  declarations: []
})
export class AppRoutingModule { }
