import { TestBed, inject } from '@angular/core/testing';

import { PlateraService } from './platera.service';

describe('PlateraService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [PlateraService]
    });
  });

  it('should be created', inject([PlateraService], (service: PlateraService) => {
    expect(service).toBeTruthy();
  }));
});
