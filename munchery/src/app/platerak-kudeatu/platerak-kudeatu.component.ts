import { Component, OnInit } from '@angular/core';
import { PlateraService } from './../platera.service';
import { Platera } from './../platera';

@Component({
  selector: 'app-platerak-kudeatu',
  templateUrl: './platerak-kudeatu.component.html',
  styleUrls: ['./platerak-kudeatu.component.css']
})
export class PlaterakKudeatuComponent implements OnInit {

  constructor(private platera: PlateraService) { }

  platerak: Platera[];
  
  getPlaterak(): void {     
	this.platera.ListPlaterak()        
			.subscribe(platerak => this.platerak = platerak);    
  }

  PlateraEzabatu(id: any): void {    
	this.platera.DeletePlatera(id)    
		.subscribe( res => {console.log(res);},        
				     err => {console.log("Errorea gertatu da");}    
				);    
  }

  ngOnInit() {
    this.getPlaterak();
  }

}
