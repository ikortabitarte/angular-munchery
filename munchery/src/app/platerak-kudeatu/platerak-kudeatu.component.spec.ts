import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PlaterakKudeatuComponent } from './platerak-kudeatu.component';

describe('PlaterakKudeatuComponent', () => {
  let component: PlaterakKudeatuComponent;
  let fixture: ComponentFixture<PlaterakKudeatuComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PlaterakKudeatuComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PlaterakKudeatuComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
