import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BerriaComponent } from './berria.component';

describe('BerriaComponent', () => {
  let component: BerriaComponent;
  let fixture: ComponentFixture<BerriaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BerriaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BerriaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
