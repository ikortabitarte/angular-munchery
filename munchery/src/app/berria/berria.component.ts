import { Component, OnInit } from '@angular/core';
import { PlateraService } from './../platera.service';
import { Platera } from './../platera';

@Component({
  selector: 'app-berria',
  templateUrl: './berria.component.html',
  styleUrls: ['./berria.component.css']
})
export class BerriaComponent implements OnInit {

  constructor(private plateraService: PlateraService) { }

  platera: Platera= {       
	_id: '',
	nombre: '',        
	precio: 0,        
	imagen: '',        
	stock: 0,       
	data: new Date(Date.now())    
  };    

  PlateraBerria(): void {    
	this.plateraService.AddPlatera(this.platera)    
		.subscribe( res => {console.log(res); },        
				err => {console.log("Errorea gertatu da");}   
				);    
	}

  ngOnInit() {
  }

}
