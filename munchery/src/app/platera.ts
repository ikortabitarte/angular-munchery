export class Platera {
    _id: string;    
	nombre: string;    
	precio: number;    
	imagen: string;    
	stock: number;    
	data: Date;
}
