import { Component, OnInit } from '@angular/core';
import { PlateraService } from './../platera.service';
import { CartService } from './../cart.service';
import { Platera } from './../platera';

@Component({
  selector: 'app-menua',
  templateUrl: './menua.component.html',
  styleUrls: ['./menua.component.css']
})
export class MenuaComponent implements OnInit {

  constructor(private platera: PlateraService, private cartService: CartService) { }

  platerak: Platera[];
  
  getPlaterak(): void {     
	this.platera.ListPlaterak()        
			.subscribe(platerak => this.platerak = platerak);    
  }
  
  PlateraGehitu(platera: Platera): void {
    this.cartService.PlateraGehitu(platera)
  }

  ngOnInit() {
    this.getPlaterak();
  }

}
